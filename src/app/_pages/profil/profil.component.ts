import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  user: any
  constructor(private _userSrv: UserService) { 
    this.user= this._userSrv.getUser()
  }

  ngOnInit(): void {
  }

}
