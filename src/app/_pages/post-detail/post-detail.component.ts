import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/_services/post.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  post: any = {}
  comments: any = []
  showComments = false
  postLength = 0
  constructor(private _postSrv: PostService, private _route: ActivatedRoute, private _userSrv: UserService) { 
    this._route.params.subscribe(param => {
      if (param) 
      this.getPostDetail(param.id)
      this.getComments(param.id)
    })
  }

  ngOnInit(): void {
  }

  getPostDetail(id) {
    this._postSrv.getPostId(id).subscribe((data: {}) => {
      this.post = data
      this.postLength = Object.keys(data).length
      this.post.username = this._userSrv.filterUser(this.post.userId)
    })
  }

  getComments(id) {
    this._postSrv.getAllComments(id).subscribe((data: {}) => {
      this.comments = data
    })
  }

  showComment() {
    this.showComments = !this.showComments    
  }

}
