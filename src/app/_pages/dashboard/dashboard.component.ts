import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from 'src/app/_services/post.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  userData: any
  posts: any = []
  tempPost: any = []
  page = 1
  pageSize = 10
  usersData: any
  postList: any = []
  constructor(private _router: Router, private _userSrv: UserService, private _postSrv: PostService) { 
    this.userData = this._userSrv.getUser()
    this.usersData = this._userSrv.getUsers()
  }
  
  ngOnInit(): void {
    this.getPost()
  }
  
  getPost() {
    this._postSrv.getPosts().subscribe((data: {}) => {
      this.tempPost = data
      this.postList = data
      for (let i in this.tempPost) {
        this.getCommentLength(this.tempPost[i].id).then(val => this.tempPost[i].totalComment = val)
      }
      this.posts = this.tempPost.slice(0, this.pageSize)
    }, err => {
      console.log(err)
    })
  }


  getCommentLength(postId: any) {
    return this._postSrv.getComment(postId).then((data: []) => {
      return data.length 
    })
    
  }

  onPageChange(page: number) {
    this.page = page
    if (this.page !== 1) {
      this.posts = this.tempPost.slice((this.page - 1) * this.pageSize, this.page * this.pageSize)
    }
  }

  toDetail(id: any) {
    this._router.navigate([`/detail/${id}`])
  }

  onSearch(event) {
    const val = event.target.value;
    const query = val.toLowerCase()
    this.postList = this.tempPost.filter(
      (item) => item.body.toLowerCase().indexOf(query) >= 0
    );
    this.posts = this.postList.slice(0, this.pageSize)
  }

}
