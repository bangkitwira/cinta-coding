import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
 
  loginGroup: FormGroup;
  username: FormControl;
  password: FormControl;
  Users: any = []
  loading = false
  message=""
  isError = false
  constructor(private _fb: FormBuilder, private _userSrv: UserService, private _router: Router) { 
    this.username = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);
    this.loginGroup = this._fb.group({
      usernameCtrl: this.username,
      passwordCtrl: this.password
    })
  }

  ngOnInit(): void {
  }

  doLogin() {
    this.loading = true
    this._userSrv.findUser().subscribe((data: {}) => {
        this.Users = data
        this._userSrv.setUsers(data)
        const user = this.Users.filter(user => user.username === this.username.value);
        if (user.length !== 0) {
          this.loading = false
          this.isError = false
          this._userSrv.setUser(user[0])
          this._router.navigate(['/'])
        } else {
          this.loading = false;
          this.isError = true
          this.message= "There is no user with given credentials"
        }
          
    }, err => {
      console.log(err)
    })
  }

  closeAlert() {
    this.isError = false
  }

}
