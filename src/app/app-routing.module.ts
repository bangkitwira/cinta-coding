import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guard/auth.guard';
import { DashboardComponent } from './_pages/dashboard/dashboard.component';
import { LandingComponent } from './_pages/landing/landing.component';
import { LoginComponent } from './_pages/login/login.component';
import { PostDetailComponent } from './_pages/post-detail/post-detail.component';
import { ProfilComponent } from './_pages/profil/profil.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }, 
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'detail/:id',
    component: PostDetailComponent
  },
  {
    path: 'profile',
    component: ProfilComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
