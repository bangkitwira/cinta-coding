import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './_pages/landing/landing.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './_pages/login/login.component';
import { DashboardComponent } from './_pages/dashboard/dashboard.component';
import { HeaderComponent } from './_shared/header/header.component';
import { PostDetailComponent } from './_pages/post-detail/post-detail.component';
import { ProfilComponent } from './_pages/profil/profil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './_services/user.service';
import { AuthGuard } from './_guard/auth.guard';
import { GlobalService } from './_services/global.service';
import { PostService } from './_services/post.service';
import { CardPostComponent } from './_shared/_components/card-post/card-post.component';
import { SkeletonLoaderComponent } from './_shared/_components/skeleton-loader/skeleton-loader.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    PostDetailComponent,
    ProfilComponent,
    CardPostComponent,
    SkeletonLoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule
  ],
  providers: [
    UserService,
    AuthGuard,
    GlobalService,
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
