import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userData: any
  constructor(private _router: Router, private _userSrv: UserService) { 
    this.userData = this._userSrv.getUser()
  }

  ngOnInit(): void {
  }

  toProfile() {
    this._router.navigate(['profile'])
  }
}
