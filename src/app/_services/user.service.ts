import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';

@Injectable()
export class UserService {

  user = new BehaviorSubject({})
  userData: any
  api_url
  constructor(private _http: HttpClient, private _gs: GlobalService) {
    this.api_url = this._gs.API_URL
  }

  findUser() {
    return this._http.get(this.api_url + 'users', this._gs.httpHeader)
      .pipe(
        retry(1),
        catchError(this._gs.httpError)
      )
  }

  setUser(data: any) {
    this.user.next(data)
    localStorage.setItem('user', JSON.stringify(data))
  }

  setUsers(data: any) {
    localStorage.setItem('users', JSON.stringify(data))
  }

  getUsers() {
    return JSON.parse(localStorage.getItem('users'))
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'))
  }

  isAuthenticated() {
    const user = this.getUser()
    if (user) {
      return true
    } else {
      return false
    }
  }

  logoutUser() {
    localStorage.removeItem('user')
  }

  
  filterUser(userId) {
    const users = this.getUsers()
    const user = users.filter(user => user.id === userId)
    return user[0].username
  }

}
