import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { retry, catchError } from 'rxjs/operators';


@Injectable()
export class PostService {
  api_url
  constructor(private _http: HttpClient, private _gs: GlobalService) { 
    this.api_url = this._gs.API_URL
  }

  getPosts() {
    return this._http.get(this.api_url + 'posts', this._gs.httpHeader)
      .pipe(
        retry(1),
        catchError(this._gs.httpError)
      )
  }

  getPostId(id) {
    return this._http.get(this.api_url + `posts/${id}`, this._gs.httpHeader)
    .pipe(
      retry(1),
      catchError(this._gs.httpError)
    )
  }

  getComment(postId: any) {
    return this._http.get(this.api_url + `posts/${postId}/comments`, this._gs.httpHeader)
      .pipe(
        retry(1),
        catchError(this._gs.httpError)
      ).toPromise()
  }

  getAllComments(postId) {
    return this._http.get(this.api_url + `posts/${postId}/comments`, this._gs.httpHeader)
    .pipe(
      retry(1),
      catchError(this._gs.httpError)
    )
  }
}
