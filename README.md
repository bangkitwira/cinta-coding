# CintaCoding

This project was generated with Angular to create simple tests, this project contains login, dashboard, post details, and show comments on post details, also viewing user profile. There is also search and pagination within this apps.

## Development server

Run `npm install` to install all dependencies and then run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further Notes
This apps using additional libraries like `ng-bootstrap` and `ngx-skeleton-loader` to provide better UI, and separate into few modul, like `_pages` that contains pages component, `_services` that contain **API** communications with **jsonplaceholder**, `_guard` that contains router guard session also `_shared` that contains reusable components along this apps

